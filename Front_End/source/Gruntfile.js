module.exports = function(grunt) {

  grunt.initConfig({


    // Sass object
    sass: {
      dev: {
      	files: {
      		'assets/stylesheets/style.css': 'assets/stylesheets/style.css.scss',
      		'assets/stylesheets/legacy/screen.css': 'assets/stylesheets/legacy/screen.css.scss'
      	},
        options: {
          style: 'expanded',
          compass: true
        }
      }
    },

    // CSS Minify
    // cssmin: {
  	// options: {
    // 		shorthandCompacting: false,
    // 		roundingPrecision: -1
  	// },
  	// target: {
    // 		files: {
    //   			'assets/stylesheets/combined.css': ['assets/stylesheets/style.css', 'assets/stylesheets/legacy/screen.css']
    // 		}
  	// }
    // },

    // Autoprefixer
    autoprefixer: {
      options: {
        // Task-specific options go here.
      },
      your_target: {
        src: 'assets/stylesheets/style.css',
        dest: 'assets/stylesheets/style.css'
      },
    },



  });

  //build task
  grunt.registerTask('build', ['sass', 'autoprefixer', 'cssmin', 'concat']);

  // Load up tasks
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
};
