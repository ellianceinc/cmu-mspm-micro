// init ScrollMagic controller
var controller = new ScrollMagic.Controller();



//

// build scene 0
// var scene = new ScrollMagic.Scene({
// 	triggerElement: "#curtain-over",
// 	triggerHook: "0",
// 	duration: "100%"
// })
// .setPin("#curtain-under")
// // add indicators (requires plugin)
// .addIndicators({ name: "hero" })
// .addTo(controller);



// Line 1
var scene = new ScrollMagic.Scene({
	triggerElement: "#section1",
	duration: "100%"
})
// animate 1
.setTween("#line-one", {
	height: "130"
})
.addTo(controller);


// Line 2
var scene = new ScrollMagic.Scene({
	triggerElement: "#section2",
	duration: "100%"
})
// animate 1
.setTween("#line-two", {
	height: "400"
})
.addTo(controller);


// Line 3
var scene = new ScrollMagic.Scene({
	triggerElement: "#section3",
	duration: "100%"
})
.setTween("#line-three", {
	height: "200"
})
.addTo(controller);


// Line 4
var scene = new ScrollMagic.Scene({
	triggerElement: "#section4",
	duration: "100%"
})
.setTween("#line-four", {
	height: "140"
})
.addTo(controller);


// Line 5
var scene = new ScrollMagic.Scene({
	triggerElement: "#section5",
	duration: "100%"
})
// animate 1
.setTween("#line-five", {
	height: "210"
})
.addTo(controller);
///////////////////////Nav Offcanvas///////////////////////
$('.trigger').click(function() {
  var $this = $(this),
  notThis = $this.hasClass('open'),
  thisNav = $this.attr("rel"),
  navLink = $('.jump-link');
  //close if you click another menu trigger
  if (!notThis) {
    $('.triggered-area, .trigger').removeClass('open');
    if ( $( '.trigger' ).hasClass( "block-scroll" ) ){
      $('html').removeClass('disable-scroll');
      $('#overlay-mobile').removeClass('visible');
    }
  }
  //open the nav
  $this.toggleClass('open');
  $("#"+thisNav).toggleClass('open');
  if ( $( this ).hasClass( "block-scroll" ) ){
    $('html').toggleClass('disable-scroll');
    $('#overlay-mobile').toggleClass('visible');
  }
});

//Close menu to reveal scrolling
$('.jump-link').click(function(){
  $('.triggered-area, .trigger').removeClass('open');
  if ( $( '.trigger' ).hasClass( "block-scroll" ) ){
    $('html').removeClass('disable-scroll');
    $('#overlay-mobile').removeClass('visible');
  }
});
//close if you click on anything but this nav item or a trigger
// $(document).on('click', function(event) {
//   if (!$(event.target).closest('.triggered-area, .trigger').length) {
//     $('.triggered-area, .trigger').removeClass('open');
//     if ( $( '.trigger' ).hasClass( "block-scroll" ) ){
//       $('html').removeClass('disable-scroll');
//       $('#overlay-mobile').removeClass('visible');
//     }
//   }
// });

// //Close button
// $('.offcanvas-close-button').click(function() {
//   $('.triggered-area, .trigger').removeClass('open');
//   $('html').removeClass('disable-scroll');
//   $('#overlay-mobile').removeClass('visible');
// });
$('.section-4--carousel').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  dots: false,
  prevArrow: $('.section-4--prev'),
  nextArrow: $('.section-4--next'),
  responsive: [
    {
      breakpoint: 920,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 760,
      settings: {
        slidesToShow: 2.25,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.section-4--carousel-students').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: false,
    prevArrow: $('.section-4--students-prev'),
    nextArrow: $('.section-4--students-next'),
    responsive: [
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

$('.logo-carousel.internships .logo--section').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    prevArrow: $('.logo-carousel.internships .logo--prev'),
    nextArrow: $('.logo-carousel.internships .logo--next'),
    responsive: [
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
});

$('.logo-carousel.capstones .logo--section').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    prevArrow: $('.logo-carousel.capstones .logo--prev'),
    nextArrow: $('.logo-carousel.capstones .logo--next'),
    responsive: [
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
});

$('.logo-carousel.hired .logo--section').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    prevArrow: $('.logo-carousel.hired .logo--prev'),
    nextArrow: $('.logo-carousel.hired .logo--next'),
    responsive: [
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
});

$('.capstone-carousel').slick({
  prevArrow: $('.capstone-carousel--button.button--prev'),
  nextArrow: $('.capstone-carousel--button.button--next'),

});

$('.careers--carousel').slick({
  prevArrow: $('.careers--carousel--button.prev'),
  nextArrow: $('.careers--carousel--button.next'),
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 920,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 760,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
})
;
// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top - 0
        }, 1000);
      }
    }
  });
//http://web-accessibility.carnegiemuseums.org/code/accordions/

var accordionButtons = $('.accordion-controls li button');

function accordionToggle() {
  $('.accordion-controls li button').on('click', function(e) {
    $control = $(this);

    accordionContent = $control.attr('aria-controls');
    // checkOthers($control[0]);

    isAriaExp = $control.attr('aria-expanded');
    newAriaExp = (isAriaExp == "false") ? "true" : "false";
    $control.attr('aria-expanded', newAriaExp);

    isAriaHid = $('#' + accordionContent).attr('aria-hidden');
    if (isAriaHid == "true") {
      $(this).next('div').attr('aria-hidden', "false");
      $(this).next('div').css('display', 'block');
    } else {
      $(this).next('div').attr('aria-hidden', "true");
      $(this).next('div').css('display', 'none');
    }
  });
};


//call this function on page load
accordionToggle();


//close accorions by default
function checkInitialState() {
  for (var i=0; i<accordionButtons.length; i++) {
    if (($(accordionButtons[i]).attr('aria-expanded')) == 'false') {
      $(accordionButtons[i]).next('div').css('display', 'none');
    }
  }
};
checkInitialState();
//Scroll to top link
var $toplink = $('.back-to-top');
$toplink.click(function() {
    $('html, body').animate({
        scrollTop: $('body').offset().top
    }, 500);
});
var rellax = new Rellax('.rellax', {
  speed: 0.5,
  center: true,
  round: false,
  vertical: true,
  horizontal: false
});
document.querySelector('.spring-semester-modal').addEventListener('click', function() {
  new Modal({el: document.getElementById('spring-semester')}).show();
});


document.querySelector('.summer-semester-modal').addEventListener('click', function() {
  new Modal({el: document.getElementById('summer-semester')}).show();
});


document.querySelector('.fall-semester-modal').addEventListener('click', function() {
  new Modal({el: document.getElementById('fall-semester')}).show();
});











