// init ScrollMagic controller
var controller = new ScrollMagic.Controller();



//

// build scene 0
// var scene = new ScrollMagic.Scene({
// 	triggerElement: "#curtain-over",
// 	triggerHook: "0",
// 	duration: "100%"
// })
// .setPin("#curtain-under")
// // add indicators (requires plugin)
// .addIndicators({ name: "hero" })
// .addTo(controller);



// Line 1
var scene = new ScrollMagic.Scene({
	triggerElement: "#section1",
	duration: "100%"
})
// animate 1
.setTween("#line-one", {
	height: "130"
})
.addTo(controller);


// Line 2
var scene = new ScrollMagic.Scene({
	triggerElement: "#section2",
	duration: "100%"
})
// animate 1
.setTween("#line-two", {
	height: "400"
})
.addTo(controller);


// Line 3
var scene = new ScrollMagic.Scene({
	triggerElement: "#section3",
	duration: "100%"
})
.setTween("#line-three", {
	height: "200"
})
.addTo(controller);


// Line 4
var scene = new ScrollMagic.Scene({
	triggerElement: "#section4",
	duration: "100%"
})
.setTween("#line-four", {
	height: "140"
})
.addTo(controller);


// Line 5
var scene = new ScrollMagic.Scene({
	triggerElement: "#section5",
	duration: "100%"
})
// animate 1
.setTween("#line-five", {
	height: "210"
})
.addTo(controller);
