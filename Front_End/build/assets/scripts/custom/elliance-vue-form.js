// form_id = 10 can be used when testing.
function elliance_start_form(form_id) {

var vueapp = new Vue({
  el: '#request-info',
  data: {
    messages: [],
    formData: {},
    buttonLabel: "Submit",
    isSubmitting: false,
    formSubmitted: false
  },
  methods: {
    submitForm: function () {
    	// don't allow dupes come in. "locking" the form if it is in the process of submitting. lock will clear after the POST request ends.
    	if (this.isSubmitting) {
    		//console.log("form is already submitted");
    		return;
    	}
    	this.isSubmitting = true;

    	var builtinData = {};
    	//builtinData.form_id = "10";
    	builtinData.form_id = form_id;
    	builtinData.email2 = "";
    	builtinData.submitted_on = Date.now();
        // builtinData.url = window.location.href;
        // builtinData.domain = window.location.hostname;
        /// include any query params included with the url.
        window.location.search.replace('?', '').split('&').forEach(function (el) {
            let chunks = el.split('=');
            //console.log(chunks, this.formData);
            builtinData[chunks[0]] = chunks[1];
        });
    	var sendingData = builtinData;
    	//Object.assign({}, builtinData, this.formData);

      // random field order
    	// for (var key in this.formData) {
    	// 	sendingData[key] = this.formData[key];
    	// }
      // console.log("submitted the form.", Object.keys(sendingData), sendingData);

      // explicity specify the field order
      sendingData["first_name"] = this.formData["first_name"]
      sendingData["last_name"] = this.formData["last_name"]
      sendingData["company"] = this.formData["company"]
      sendingData["title"] = this.formData["title"]
      sendingData["email"] = this.formData["email"]
      sendingData["phone"] = this.formData["phone"]
      sendingData["interest"] = this.formData["interest"]
      sendingData["opportunities"] = this.formData["opportunities"]
      sendingData["url"] = window.location.href;
      sendingData["domain"] = window.location.hostname;

    	this.buttonLabel = "Submitting";

    	let that = this;
    	// debugging URL if running against local lead engine.
    	//let url = 'http://localhost:7071/api/leadsubmit';
    	let url = 'https://elliance-leads.azurewebsites.net/api/leadsubmit?code=XpcaswgZaRUYnJOdjkxhlhFuax7IHEx0J/FEasXQSDme/JwbBX4wPg==';
      	fetch(
      		url,
      		{
  				method: 'POST',
  				headers: {
    				'Content-Type': 'application/json'
  				},
  				body: JSON.stringify(sendingData)
			}
		)
		.then(res => res.json())
    	.then(json => {
			that.isSubmitting = false;
  			let passed = true;
  			if (passed) {
  				console.log(json);
  				if (json.redirect != "") {
  					window.location = json.redirect;
  				}
  				that.formSubmitted = true;
  			}
    		this.buttonLabel = "Submit";
    	})
		.catch(function(ex) {
			that.isSubmitting = false;
    		console.log('posting failed', ex)
  			that.formSubmitted = true;
    		this.buttonLabel = "Submit";
  		});
    }
  }
});

}
;
