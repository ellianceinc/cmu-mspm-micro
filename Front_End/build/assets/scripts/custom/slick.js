$('.section-4--carousel').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  dots: false,
  prevArrow: $('.section-4--prev'),
  nextArrow: $('.section-4--next'),
  responsive: [
    {
      breakpoint: 920,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 760,
      settings: {
        slidesToShow: 2.25,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.section-4--carousel-students').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: false,
    prevArrow: $('.section-4--students-prev'),
    nextArrow: $('.section-4--students-next'),
    responsive: [
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

$('.logo-carousel.internships .logo--section').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    prevArrow: $('.logo-carousel.internships .logo--prev'),
    nextArrow: $('.logo-carousel.internships .logo--next'),
    responsive: [
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
});

$('.logo-carousel.capstones .logo--section').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    prevArrow: $('.logo-carousel.capstones .logo--prev'),
    nextArrow: $('.logo-carousel.capstones .logo--next'),
    responsive: [
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
});

$('.logo-carousel.hired .logo--section').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    prevArrow: $('.logo-carousel.hired .logo--prev'),
    nextArrow: $('.logo-carousel.hired .logo--next'),
    responsive: [
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
});

$('.capstone-carousel').slick({
  prevArrow: $('.capstone-carousel--button.button--prev'),
  nextArrow: $('.capstone-carousel--button.button--next'),

});

$('.careers--carousel').slick({
  prevArrow: $('.careers--carousel--button.prev'),
  nextArrow: $('.careers--carousel--button.next'),
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 920,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 760,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
})
;
