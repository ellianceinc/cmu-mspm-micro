var easeBoxes = [];
document.querySelectorAll('.easeBox').forEach(function (elem, i) {
  var timing = elem.getAttribute('data-timing');
  easeBoxes.push(basicScroll.create({
    elem: elem,
    from: 'middle-bottom',
    to: 'bottom-top',
    direct: true,
    props: {
      '--ty': {
        from: '0',
        to: '100px',
        timing: timing
      }
    }
  }));
});
easeBoxes.forEach(function (easeBox) {
  return easeBox.start();
});
