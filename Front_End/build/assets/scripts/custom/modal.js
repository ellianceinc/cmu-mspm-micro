document.querySelector('.spring-semester-modal').addEventListener('click', function() {
  new Modal({el: document.getElementById('spring-semester')}).show();
});


document.querySelector('.summer-semester-modal').addEventListener('click', function() {
  new Modal({el: document.getElementById('summer-semester')}).show();
});


document.querySelector('.fall-semester-modal').addEventListener('click', function() {
  new Modal({el: document.getElementById('fall-semester')}).show();
});
