// init controller
var controller = new ScrollMagic.Controller();

$(function() {
	// wait for document ready
	// build scene
	var scene = new ScrollMagic.Scene({
		triggerElement: "#curtain-over",
		triggerHook: "0",
		duration: "100%"
	})
		.setPin("#curtain-under")
		// add indicators (requires plugin)
		.addIndicators({ name: "1" })
		.addTo(controller);
});
