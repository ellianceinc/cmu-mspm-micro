var rellax = new Rellax('.rellax', {
  speed: 0.5,
  center: true,
  round: false,
  vertical: true,
  horizontal: false
});
